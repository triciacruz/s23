/*// console.log("hello")

	
	let trainer = {
		name: 'Ash Ketchun',
		age: 10,
		pokemons:['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
		friends: {
			hoenn: ['May', 'Max'],
			kanto: ['Brock', 'Misty']
		},
		talk: function() {
			console.log('Pikachu! I choose you!');
		}
	}
	console.log(trainer);
	console.log('Result of dot note notation:');
	console.log(trainer.name);
	console.log('Result of square bracket notation:');
	console.log(trainer['pokemons']);
	console.log('Result of talk method:');
	trainer.talk();

	function Pokemon(name, level){
			this.name = name; // pikachu
			this.level = level; // level 3
			this.health = 2 * level; // 6 health
			this.attack = level;

			//tackle = attack
			this.tackle = function(target){
				console.log(this.name + ' tackled ' + target.name);

				target.health -= this.attack;
				console.log(target.name + "'s heath is now reduced to " + target.health);
			};

			this.faint = function(){
					if (target.health <= 0){
						console.log(target.name + ' fainted');
					}else (target.health > 0)
						console.log(target.name + "'s heath is now reduced to " + target.health);
				} 	
		}

		let pikachu = new Pokemon("Pikachu", 12);
		let geodude = new Pokemon('Geodude', 8);
		let newtwo = new Pokemon('Newtwo', 100);

		console.log(pikachu);
		console.log(geodude);
		console.log(newtwo);

		geodude.tackle(pikachu);
		newtwo.tackle(geodude);
	
*/

// console.log("hello")

	
	let trainer = {
		name: 'Ash Ketchun',
		age: 10,
		pokemons:['Pikachu', 'Charizard', 'Squirtle', 'Bulbasaur'],
		friends: {
			hoenn: ['May', 'Max'],
			kanto: ['Brock', 'Misty']
		},
		talk: function() {
			console.log('Pikachu! I choose you!');
		}
	}
	console.log(trainer);
	console.log('Result of dot note notation:');
	console.log(trainer.name);
	console.log('Result of square bracket notation:');
	console.log(trainer['pokemons']);
	console.log('Result of talk method:');
	trainer.talk();

	function Pokemon(name, level){
			this.name = name; // pikachu
			this.level = level; // level 3
			this.health = 2 * level; // 6 health
			this.attack = level;

			//tackle = attack
			this.tackle = function(target){
				console.log(this.name + ' tackled ' + target.name);

				target.health -= this.attack;

				if (target.health > 0){
						console.log(target.name + "'s heath is now reduced to " + target.health);
					}else  (target.health <= 0)
						console.log(target.name + ' fainted');
			};
		}

		let pikachu = new Pokemon("Pikachu", 12);
		let geodude = new Pokemon('Geodude', 8);
		let newtwo = new Pokemon('Newtwo', 100);

		console.log(pikachu);
		console.log(geodude);
		console.log(newtwo);

		geodude.tackle(pikachu);
		newtwo.tackle(geodude);
		console.log(geodude);
	
